package com.kodebonek.funswipe;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.List;

import uk.co.senab.photoview.PhotoView;

public class MainActivity extends ActionBarActivity {

    MyPagerAdapter mPagerAdapter;
    HackyViewPager mViewPager;
    SelectAgainSpinner spin1,spin2;
    JSONArray jsonData;
    //private static final String API_URL = "http://www.reddit.com/r/funny/hot/.json?limit=15";
    private static final String API_URL = "http://api.kodebonek.com/funswipe/query.php";
    String search_mode = "all";
    String time_mode = "newest";
    static boolean is_light;
    private boolean userIsInteracting = false;
    private boolean doubleBackToExitPressedOnce;
    static int deviceWidth;
    private static String TAG = "mainActivity";
    private boolean queryInProgress = false;
    private static SharedPreferences settings = null;
    private int prevOrientation;
    static ProgressDialog mIndeterminateProgressDialog;
    public static Typeface titleTypeFace,metaTypeFace,contentTypeFace;
    private boolean bookmarkMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(savedInstanceState != null) {
            is_light = savedInstanceState.getBoolean("is_light");
            try {
                String jsonString = savedInstanceState.getString("json");
                jsonData = new JSONArray(jsonString);
                bookmarkMode = savedInstanceState.getBoolean("bookmark_mode");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            is_light = true;
            jsonData = null;
            loadSettings();
        }

        //set theme before content view -- MUST!
        if (is_light)
            setTheme(R.style.AppTheme);
        else
            setTheme(R.style.AppTheme_Dark);
        setContentView(R.layout.activity_main);

        setupViewPager();

        //load visited and fave database
        VisitedDB.getInstance().setContext(getApplicationContext());
        VisitedDB.getInstance().load();
        BookmarkDB.getInstance().setContext(getApplicationContext());
        BookmarkDB.getInstance().load();
        RaterLogic.getInstance().setContext(getApplicationContext());
        RaterLogic.getInstance().init();

        Toolbar actionBar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(actionBar);
        setupSpinner();
        //setupIndeterminateProgressDialog(); --> can cause crash on startup hmm
        setupTheme();

        if(savedInstanceState == null) {
            //default query -> newest , all , non-favourite
            queryReddit(0);
        } else {
            //reload json data
            mPagerAdapter.updateData(jsonData);
        }

        //set typeface
        if (titleTypeFace==null) titleTypeFace = Typeface.createFromAsset(getAssets(), "font/roboto-slab/RobotoSlab-Bold.ttf");
        if (metaTypeFace==null) metaTypeFace = Typeface.createFromAsset(getAssets(), "font/roboto-slab/RobotoSlab-Light.ttf");
        if (contentTypeFace==null) contentTypeFace = Typeface.createFromAsset(getAssets(), "font/roboto-slab/RobotoSlab-Regular.ttf");
        //if (contentTypeFace==null) contentTypeFace = Typeface.createFromAsset(getActivity().getAssets(), "font/Bitstream-Vera-Sans/Vera.ttf");

        //get device width for sizing the bitmap
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;

        //set onclick to change theme
        ImageView vi = (ImageView) findViewById(R.id.imageChangeTheme);
        vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_light = !is_light;
                settings.edit().putBoolean("is_light",is_light).commit();
                recreate();
            }
        });

        //Ion.getDefault(getApplicationContext()).configure().setLogging("ION", Log.DEBUG);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void setupIndeterminateProgressDialog() {
        mIndeterminateProgressDialog = new ProgressDialog(MainActivity.this,R.style.MyProgressDialog);

        mIndeterminateProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mIndeterminateProgressDialog.setIndeterminate(true);
        mIndeterminateProgressDialog.setCancelable(false);
        //mIndeterminateProgressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.my_progress_indeterminate));
    }

    private void loadSettings() {
        if (settings==null)
            settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        is_light = settings.getBoolean("is_light",true);

        Log.d(TAG, "settings loaded: " + settings.getAll().toString());
    }

    private void setupViewPager() {

        //mPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (HackyViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(1);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                int currentTotal = mPagerAdapter.getCount();
                if (currentTotal == 0)
                    return;

                //5 page before total -> load stuff in background
                if (position + 5 > currentTotal) {
                    Log.d(TAG, "current:" + position + ", total:" + currentTotal);
                    queryReddit(currentTotal);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setupSpinner() {

        //setup spinner , all/images/jokes
        spin1 = (SelectAgainSpinner) findViewById(R.id.spinner_mode);
        spin1.setAdapter(ArrayAdapter.createFromResource(this,
                R.array.menu_spinner_mode,
                android.R.layout.simple_spinner_dropdown_item));

        spin1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (userIsInteracting) {
                    String mode = parent.getItemAtPosition(position).toString();
                    if (mode.equalsIgnoreCase("all")) search_mode = "all";
                    else if (mode.equalsIgnoreCase("images only")) search_mode = "images";
                    else if (mode.equalsIgnoreCase("jokes only")) search_mode = "jokes";
                    else if (mode.equalsIgnoreCase("bookmarks")) search_mode = "bookmarks";

                    spin2 = (SelectAgainSpinner) findViewById(R.id.spinner_time);
                    if (spin2!=null) {
                        if (search_mode == "bookmarks") {
                            bookmarkMode = true;
                            spin2.setEnabled(false);
                            spin2.setClickable(false);
                        } else {
                            bookmarkMode = false;
                            spin2.setEnabled(true);
                            spin2.setClickable(true);
                        }
                    }

                    queryReddit(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spin2 = (SelectAgainSpinner) findViewById(R.id.spinner_time);
        spin2.setAdapter(ArrayAdapter.createFromResource(this,
                R.array.menu_spinner_time,
                android.R.layout.simple_spinner_dropdown_item));
        spin2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (userIsInteracting) {
                    time_mode = parent.getItemAtPosition(position).toString();
                    queryReddit(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (bookmarkMode) {
            spin2.setEnabled(false);
            spin2.setClickable(false);
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userIsInteracting = true;
    }

    private void setupTheme() {

/*
        LayerDrawable layer1 = (LayerDrawable) findViewById(R.id.spinner_time).getBackground();
        GradientDrawable bg1 = (GradientDrawable) layer1.getDrawable(0);
        LayerDrawable layer2 = (LayerDrawable) findViewById(R.id.spinner_mode).getBackground();
        GradientDrawable bg2 = (GradientDrawable) layer2.getDrawable(0);
*/
        if (is_light) {
            ((View) findViewById(R.id.pager)).setBackgroundColor(getResources().getColor(R.color.app_background_color));
            ((View) findViewById(R.id.mytoolbar)).setBackgroundColor(getResources().getColor(R.color.app_background_color));
            ((SelectAgainSpinner) findViewById(R.id.spinner_mode)).setPopupBackgroundResource(R.color.app_background_color);
            ((SelectAgainSpinner) findViewById(R.id.spinner_time)).setPopupBackgroundResource(R.color.app_background_color);
            ((ImageView) findViewById(R.id.imageChangeTheme)).setImageDrawable(getResources().getDrawable(R.drawable.ic_invert));

/*
            bg1.setStroke(2,getResources().getColor(R.color.app_spinner_border));
            bg1.setColor(getResources().getColor(R.color.app_background_color));

            bg2.setStroke(2,getResources().getColor(R.color.app_spinner_border));
            bg2.setColor(getResources().getColor(R.color.app_background_color));
*/

        } else {
            ((View) findViewById(R.id.pager)).setBackgroundColor(getResources().getColor(R.color.app_background_color_dark));
            ((View) findViewById(R.id.mytoolbar)).setBackgroundColor(getResources().getColor(R.color.app_background_color_dark));
            ((SelectAgainSpinner) findViewById(R.id.spinner_mode)).setPopupBackgroundResource(R.color.app_background_color_dark);
            ((SelectAgainSpinner) findViewById(R.id.spinner_time)).setPopupBackgroundResource(R.color.app_background_color_dark);
            ((ImageView) findViewById(R.id.imageChangeTheme)).setImageDrawable(getResources().getDrawable(R.drawable.ic_invert_dark));

/*
            bg1.setStroke(2,getResources().getColor(R.color.app_spinner_border_dark));
            bg1.setColor(getResources().getColor(R.color.app_background_color_dark));

            bg2.setStroke(2,getResources().getColor(R.color.app_spinner_border_dark));
            bg2.setColor(getResources().getColor(R.color.app_background_color_dark));
*/
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onSaveInstanceState called");
        savedInstanceState.putBoolean("is_light", is_light);
        savedInstanceState.putBoolean("bookmark_mode", bookmarkMode);
        if (jsonData!=null) {
            String jsonString = jsonData.toString();
            savedInstanceState.putString("json", jsonString);
        }

        super.onSaveInstanceState(savedInstanceState);
    }

    private void setProgressVisible(boolean visible) {

        if (visible) {

            setupIndeterminateProgressDialog();
            int rotation = getWindowManager().getDefaultDisplay().getRotation();

            switch(rotation) {
                case Surface.ROTATION_180:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                    break;
                case Surface.ROTATION_270:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                    break;
                case  Surface.ROTATION_0:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
                case Surface.ROTATION_90:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
            }

            mIndeterminateProgressDialog.show();
        } else {
            //unlock
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

            if (mIndeterminateProgressDialog!=null)
                mIndeterminateProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mIndeterminateProgressDialog != null) {
            mIndeterminateProgressDialog.dismiss();
        }
    }

    public static boolean connectedToInternet(Context context)
    {
        ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return wifi.isConnected() || mobile.isConnected();
    }

    private void showRetryDialog(String message) {

        new AlertDialog.Builder(this)
                .setMessage(message)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        queryReddit(0);
                    }
                })
                .show();
    }

    private void queryReddit(final int start) {

        if (queryInProgress) return;

        if (connectedToInternet(getApplicationContext())==false && start==0) {
            //Toast.makeText(getApplicationContext(),"Please check your internet connection",Toast.LENGTH_SHORT).show();
            showRetryDialog("Internet is required. Please check your connection.");
            return;
        }

        String URL = API_URL+"?search="+search_mode+"&mode="+time_mode+"&start="+start;
        if (bookmarkMode) {
            String bookmark_list = BookmarkDB.getInstance().getListForQuery(start,25);
            if (bookmark_list=="") {
                Log.d(TAG,"no more bookmark");
                if (start==0)
                    Toast.makeText(getApplicationContext(),"You have 0 bookmark",Toast.LENGTH_SHORT).show();

                return;
            }
            URL = URL+"&list="+bookmark_list;
        }
        Log.d(TAG,"querying : "+URL);

        AsyncHttpClient client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(2,500);
        client.setTimeout(30000);
        if (start==0)
            setProgressVisible(true);   //show on initialize only (0)

        queryInProgress = true;
        client.get(URL,
                new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject jsonObject) {

                        if (jsonObject.has("data")) {

                            JSONArray data = jsonObject.optJSONArray("data");
                            if (data == null) {
                                Log.e(TAG, "data == null !!");

                                if (start==0)
                                    Toast.makeText(getApplicationContext(), "Ooops, something bad happened with the return data!", Toast.LENGTH_SHORT).show();

                            } else {
                                Log.d(TAG, "jsonData: " + data.toString());
                                Log.d(TAG, "return data count: " + data.length());

                                if (start==0) {
                                    mPagerAdapter.updateData(data);
                                    mViewPager.setCurrentItem(0,false);   //reset to first item
                                } else {
                                    mPagerAdapter.appendData(data);
                                }
                                jsonData = mPagerAdapter.getJSONArray();
                            }
                        }

                        if (start==0) setProgressVisible(false);
                        queryInProgress = false;
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Log.e(TAG, statusCode + " " + throwable.getMessage());
                        queryInProgress = false;

                        if (start==0) {
                            setProgressVisible(false);
                            //Toast.makeText(getApplicationContext(), "Oops, something wrong with the connection. ("+statusCode +")", Toast.LENGTH_SHORT).show();
                            showRetryDialog("Oops, something wrong when connecting to server. ("+statusCode +")");
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Log.e(TAG, statusCode + " " + throwable.getMessage());
                        queryInProgress = false;

                        if (start ==0) {
                            setProgressVisible(false);
                            //Toast.makeText(getApplicationContext(), "Oops, something wrong with the connection. ("+statusCode +")", Toast.LENGTH_SHORT).show();
                            showRetryDialog("Oops, something wrong when connecting to server. ("+statusCode +")");
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu)
    {
        if(featureId == Window.FEATURE_ACTION_BAR && menu != null){
            if(menu.getClass().getSimpleName().equals("MenuBuilder")){
                try{
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                }
                catch(NoSuchMethodException e){
                    //Log.e(TAG, "onMenuOpened", e);
                }
                catch(Exception e){
                    throw new RuntimeException(e);
                }
            }
        }
        return super.onMenuOpened(featureId, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_setting) {
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            intent.putExtra("is_light",is_light);
            startActivity(intent);
            return true;
        } else if (id== R.id.action_rate) {
            //openAppRating(getApplicationContext());
            Intent intent = new Intent(getApplicationContext(), FeedbackActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public static void openAppRating(Context context) {

        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName()));
        boolean marketFound = false;

        // find all applications able to handle our rateIntent
        final List<ResolveInfo> otherApps = context.getPackageManager().queryIntentActivities(rateIntent, 0);
        for (ResolveInfo otherApp: otherApps) {
            // look for Google Play application
            if (otherApp.activityInfo.applicationInfo.packageName.equals("com.android.vending")) {

                ActivityInfo otherAppActivity = otherApp.activityInfo;
                ComponentName componentName = new ComponentName(
                        otherAppActivity.applicationInfo.packageName,
                        otherAppActivity.name
                );
                rateIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                rateIntent.setComponent(componentName);
                context.startActivity(rateIntent);
                marketFound = true;
                break;

            }
        }

        // if GP not present on device, open web browser
        if (!marketFound) {
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+context.getPackageName()));
            context.startActivity(webIntent);
        }
    }
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        JSONArray mJsonArray;
        private String TAG = "MyPagerAdapter";

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            mJsonArray = new JSONArray();
        }

        @Override
        public Fragment getItem(int position) {

            JSONObject dataObj = mJsonArray.optJSONObject(position);
            Fragment fragment = PlaceholderFragment.newInstance(getApplicationContext(),position + 1,dataObj);
            return(fragment);
        }


        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return mJsonArray.length();
        }

        public void updateData(JSONArray arr) {
            if (arr==null) return;
            mJsonArray = arr;
            notifyDataSetChanged();
        }

        public void appendData(JSONArray arr) {

            try {
                mJsonArray = concatArray(mJsonArray,arr);
                Log.d(TAG,"appendData. total now:"+mJsonArray.length());
            } catch (JSONException e) {
                //e.printStackTrace();
                Log.d(TAG,e.getMessage());
                return;
            }
            notifyDataSetChanged();
        }

        private JSONArray concatArray(JSONArray arr1, JSONArray arr2)
                throws JSONException {
            JSONArray result = new JSONArray();
            for (int i = 0; i < arr1.length(); i++) {
                result.put(arr1.get(i));
            }
            for (int i = 0; i < arr2.length(); i++) {
                result.put(arr2.get(i));
            }
            return result;
        }

        public JSONArray getJSONArray() { return mJsonArray; }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private static Context mContext;
        private static Drawable redFaveButton = null;
        JSONObject varObj;
        boolean is_bookmarked = false;
        boolean is_gif = false;
        private static String TAG = "PlaceholderFragment";
        ProgressDialog mProgress;
        //PhotoViewAttacher mAttacher;

        public static PlaceholderFragment newInstance(Context context,int sectionNumber,JSONObject dataObj) {

            mContext = context;

            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString("json",dataObj.toString());
            args.putInt("pos",sectionNumber);
            fragment.setArguments(args);

            return fragment;
        }

        public PlaceholderFragment() {
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            final Bundle args = getArguments();
            String jsonString = args.getString("json");
            try {
                varObj = new JSONObject(jsonString);
            } catch (JSONException e) {
                Log.e(TAG, e.toString());
            }
            int currentPos = args.getInt("pos");

            final String title = varObj.optString("title");
            String author = varObj.optString("author");
            String created = varObj.optString("created");
            final String content = varObj.optString("content");
            final boolean is_image = varObj.optBoolean("is_image");
            boolean is_nsfw = varObj.optBoolean("over_18");
            final String reddit_id = varObj.optString("reddit_id");
            final String reddit_url = varObj.optString("reddit_url");

            boolean visited = VisitedDB.getInstance().isVisited(reddit_id);
            is_bookmarked = BookmarkDB.getInstance().isBookmarked(reddit_id);

            String ext = content.substring(content.lastIndexOf(".") + 1);
            if (ext.equalsIgnoreCase("gif")) is_gif = true;
            else is_gif = false;

            ImageView ibShare = (ImageView) rootView.findViewById(R.id.imageShare);
            final ImageView ibFave = (ImageView) rootView.findViewById(R.id.imageFave);
            ImageView ibSave = (ImageView) rootView.findViewById(R.id.imageSave);
            ImageView ibLink = (ImageView) rootView.findViewById(R.id.imageLink);

            //listener for button share/etc
            ibShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    if (is_image) {
                        PhotoView ivImage = (PhotoView) rootView.findViewById(R.id.imageView);
                        Uri bmpUri = getLocalBitmapUri(ivImage,reddit_id);
                        if (bmpUri != null) {
                            shareIntent.setType("image/*");
                            shareIntent.putExtra(Intent.EXTRA_TEXT, title);
                            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);

                            startActivity(Intent.createChooser(shareIntent, "Share The Image"));
                        } else {
                            Log.d(TAG,"bmpURI is null?");
                        }
                    } else {
                        shareIntent.setType("text/plain");
                        String text = title + "\n\n"+content;
                        shareIntent.putExtra(Intent.EXTRA_TEXT, text);

                        startActivity(Intent.createChooser(shareIntent, "Share The Joke!"));
                    }
                }
            });

            ibFave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (is_bookmarked) {
                        BookmarkDB.getInstance().unfave(reddit_id);
                        is_bookmarked = false;
                        //Toast.makeText(rootView.getContext(),"Removed from favourite",Toast.LENGTH_SHORT).show();
                        //ibFave.getBackground().setColorFilter(null);
                        if (is_light) ibFave.setImageDrawable(getResources().getDrawable(R.drawable.ic_fave));
                        else ibFave.setImageDrawable(getResources().getDrawable(R.drawable.ic_fave_dark));
                    } else {
                        BookmarkDB.getInstance().fave(reddit_id);
                        is_bookmarked = true;
                        //Toast.makeText(rootView.getContext(),"Added to favourite",Toast.LENGTH_SHORT).show();
                        //ibFave.getBackground().setColorFilter(getResources().getColor(R.color.app_fave_color), PorterDuff.Mode.SRC_IN);
                        ibFave.setImageDrawable(getResources().getDrawable(R.drawable.ic_faved));
                    }
                }
            });

            ibSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (is_image) {

                        String ext = content.substring(content.lastIndexOf(".") + 1);
                        String filename = title.replace(" ","-")+"."+ext;
                        File file =  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);

                        Toast.makeText(mContext,"Saving ...",Toast.LENGTH_SHORT).show();
                        Ion.with(mContext)
                                .load(content)
                                .write(file)
                                .setCallback(new FutureCallback<File>() {
                                    @Override
                                    public void onCompleted(Exception e, File result) {
                                        Toast.makeText(mContext,"Save completed.",Toast.LENGTH_SHORT).show();
                                    }
                                });

                    } else {
                        Toast.makeText(mContext,"Hmm, you shouldnt be here. Save failed",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            ibLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int mode = Integer.parseInt(settings.getString("open_link_mode", "1"));
                    if (mode==1) {
                        Intent intent = new Intent(rootView.getContext(), WebViewActivity.class);
                        intent.putExtra("url", reddit_url);
                        intent.putExtra("is_light", is_light);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(reddit_url));
                        startActivity(intent);
                    }
                }
            });

            final TextView tvTitle = (TextView) rootView.findViewById(R.id.textView_title);
            tvTitle.setTypeface(titleTypeFace);
            tvTitle.setTextSize(getTitleFontSize(title.length()));
            tvTitle.setText(title);

            if (!visited) {
                tvTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showContent(rootView, varObj);
                        //save to visited db
                        VisitedDB.getInstance().add(reddit_id);
                    }
                });
            }

            TextView tvAuthor = (TextView) rootView.findViewById(R.id.textView_author);
            tvAuthor.setText(author);
            tvAuthor.setTypeface(metaTypeFace);

            TextView tvCreated = (TextView) rootView.findViewById(R.id.textView_created);
            tvCreated.setText(created);
            tvCreated.setTypeface(metaTypeFace);

            ((TextView) rootView.findViewById(R.id.tvRaterTitle)).setTypeface(contentTypeFace);

/*
            TextView tvHint = (TextView) rootView.findViewById(R.id.textView_hint);
            tvHint.setTypeface(metaTypeFace);
*/

            if (is_nsfw || is_gif) {
                TextView tvNSFW = (TextView) rootView.findViewById(R.id.textView_nsfw);
                if (is_nsfw) tvNSFW.setText("NSFW");
                else if (is_gif) tvNSFW.setText("GIF");
                tvNSFW.setTypeface(metaTypeFace);
                tvNSFW.setVisibility(View.VISIBLE);
            }
            //check already visited or not

            if (visited) {
                showContent(rootView, varObj);
            } else if (is_image) {
                //check if settings autoload images is enabled
                boolean preload = settings.getBoolean("preload_images",false);
                if (preload) {
                    Log.d(TAG,content+ " : load in background");
                    Ion.with(mContext)
                            .load(content)
                            .asBitmap()
                            .setCallback(new FutureCallback<Bitmap>() {
                                @Override
                                public void onCompleted(Exception e, Bitmap result) {
                                    Log.d(TAG,content+ " : load completed");
                                }
                            });
                }
            }

            LayerDrawable layer = (LayerDrawable) rootView.findViewById(R.id.mycontent).getBackground();
            GradientDrawable bg = (GradientDrawable) layer.getDrawable(0);
            GradientDrawable fg = (GradientDrawable) layer.getDrawable(1);

            LayerDrawable layerRater = (LayerDrawable) rootView.findViewById(R.id.app_rater).getBackground();
            GradientDrawable bgRater = (GradientDrawable) layerRater.getDrawable(0);

            //logic for theme changing
            if (is_light) {
                bg.setColor(getResources().getColor(R.color.app_background_color));
                bgRater.setColor(getResources().getColor(R.color.app_background_color));
                fg.setColor(getResources().getColor(R.color.app_window_color));

                ibShare.setImageDrawable(getResources().getDrawable(R.drawable.ic_share));
                if (is_bookmarked)
                    ibFave.setImageDrawable(getResources().getDrawable(R.drawable.ic_faved));
                else
                    ibFave.setImageDrawable(getResources().getDrawable(R.drawable.ic_fave));

                ibSave.setImageDrawable(getResources().getDrawable(R.drawable.ic_save));
                ibLink.setImageDrawable(getResources().getDrawable(R.drawable.ic_link));
            } else {
                bg.setColor(getResources().getColor(R.color.app_background_color_dark));
                bgRater.setColor(getResources().getColor(R.color.app_background_color_dark));
                fg.setColor(getResources().getColor(R.color.app_window_color_dark));

                ibShare.setImageDrawable(getResources().getDrawable(R.drawable.ic_share_dark));
                if (is_bookmarked)
                    ibFave.setImageDrawable(getResources().getDrawable(R.drawable.ic_faved));
                else
                    ibFave.setImageDrawable(getResources().getDrawable(R.drawable.ic_fave_dark));
                ibSave.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_dark));
                ibLink.setImageDrawable(getResources().getDrawable(R.drawable.ic_link_dark));
            }

            //enable save for image only
            ibSave.setEnabled(is_image);
            ibSave.setClickable(is_image);
            if (!is_image) ibSave.setAlpha((float) 0.3);

            //should we show the rater ??
            final View appRaterBox = (View) rootView.findViewById(R.id.app_rater);
            if ((currentPos % 3 == 0) && RaterLogic.getInstance().isRaterVisible()) {

                appRaterBox.setVisibility(View.VISIBLE);

                Button bPos = (Button) rootView.findViewById(R.id.buttonPositive);
                bPos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new AlertDialog.Builder(getActivity())
                                .setMessage("How about a rating on Play Store?")
                                .setNegativeButton("No, thanks", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        RaterLogic.getInstance().userRateTheApp(false);
                                        appRaterBox.setVisibility(View.GONE);
                                        Toast.makeText(mContext, "Cool.", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .setPositiveButton("Yes, sure", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        RaterLogic.getInstance().userRateTheApp(true);
                                        appRaterBox.setVisibility(View.GONE);
                                        openAppRating(mContext);
                                    }
                                })
                                .setNeutralButton("Not now", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        RaterLogic.getInstance().extendPeriod();
                                        appRaterBox.setVisibility(View.GONE);
                                    }
                                })
                                .show();
                    }
                });

                Button bNeg = (Button) rootView.findViewById(R.id.buttonNegative);
                bNeg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(getActivity())
                                .setMessage("Would you mind giving a feedback?")
                                .setNegativeButton("No, thanks", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        RaterLogic.getInstance().userSendFeedback(false);
                                        appRaterBox.setVisibility(View.GONE);
                                        Toast.makeText(mContext, "Cool.", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .setPositiveButton("Ok, sure", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        RaterLogic.getInstance().userSendFeedback(true);
                                        appRaterBox.setVisibility(View.GONE);
                                        //send a feedback
                                        Intent intent = new Intent(rootView.getContext(), FeedbackActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .setNeutralButton("Not now", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        RaterLogic.getInstance().extendPeriod();
                                        appRaterBox.setVisibility(View.GONE);
                                    }
                                })
                                .show();

                    }
                });
            }
            else {
                appRaterBox.setVisibility(View.GONE);
            }

            return rootView;
        }

        private void showContent(final View rootView,JSONObject obj) {

            final String content = varObj.optString("content");
            boolean is_image = varObj.optBoolean("is_image");
            //final String reddit_id = varObj.optString("reddit_id");

            //move title to top
            LinearLayout ll = (LinearLayout) rootView.findViewById(R.id.layoutInfo);
            ll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                    , ViewGroup.LayoutParams.WRAP_CONTENT));

            //resize the title, hide the hint
            TextView tvTitle = (TextView) rootView.findViewById(R.id.textView_title);
            tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            tvTitle.setOnClickListener(null);
//            ((TextView) rootView.findViewById(R.id.textView_hint)).setVisibility(View.GONE);

            //show 2nd toolbar
            ((Toolbar) rootView.findViewById(R.id.belowToolbar)).setVisibility(View.VISIBLE);

            if (is_image) {

                ScrollView sv = (ScrollView) rootView.findViewById(R.id.scrollView2);
                sv.setVisibility(View.VISIBLE);

                Button b = (Button) rootView.findViewById(R.id.buttonRetryImage);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadTheImage(rootView, content);
                    }
                });

                TextView tvURL = (TextView) rootView.findViewById(R.id.tvImageURL);
                tvURL.setText(content);
                tvURL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(content));
                        webIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(webIntent);
                    }
                });

                loadTheImage(rootView, content);

            } else {

                //JOKES CONTENT

                //add a nice border for title
                if (is_light) {
                    ll.setBackgroundResource(R.drawable.title_border);
                } else {
                    ll.setBackgroundResource(R.drawable.title_border_dark);
                }

                TextView tv = (TextView) rootView.findViewById(R.id.textView_content);
                tv.setTypeface(contentTypeFace);
                tv.setTextSize(getContentFontSize(content.length()));
                tv.setText(content);

                //finally, display the content
                ScrollView sv = (ScrollView) rootView.findViewById(R.id.scrollView);
                sv.setVisibility(View.VISIBLE);
            }
        }

        private void loadTheImage(final View rootView,final String imageUrl) {

            final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.VISIBLE);
            final TextView progressText = (TextView) rootView.findViewById(R.id.textProgress);
            progressText.setVisibility(View.VISIBLE);
            progressText.setTypeface(metaTypeFace);

            int resid;
            if (!is_gif) {
                resid = R.drawable.ic_placeholder;
                if (!is_light) resid = R.drawable.ic_placeholder_dark;
            } else {
                resid = R.drawable.ic_placeholder_gif;
                if (!is_light) resid = R.drawable.ic_placeholder_gif_dark;
            }

            final PhotoView imageView = (PhotoView) rootView.findViewById(R.id.imageView);
            final LinearLayout retry = (LinearLayout) rootView.findViewById(R.id.retryLayout);

            retry.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);

            Log.d(TAG, "begin loading images.." + imageUrl);
            Ion.with(mContext)
                    .load(imageUrl)
                    .progressBar(progressBar)
                    .progressHandler(new ProgressCallback() {
                        @Override
                        public void onProgress(long downloaded, long total) {
                            String totalKB = (total / 1000) + " KB";
                            int percent = (int)((downloaded * 100.0f) / total);
                            progressText.setText(""+percent+"% of "+totalKB);
                        }
                    })
                    .withBitmap()
                    .deepZoom()
                    //.smartSize(true)
                    .disableFadeIn()
                    .placeholder(resid)
                    //.error(R.drawable.ic_placeholder_error)
                    .intoImageView(imageView)
                    .setCallback(new FutureCallback<ImageView>() {
                        @Override
                        public void onCompleted(Exception e, ImageView result) {
                            progressBar.setVisibility(View.GONE);
                            progressText.setVisibility(View.GONE);

                            if (e != null) {
                                Log.e(TAG, "loading " + imageUrl + " failed: " + e.toString());
                                //Toast.makeText(mContext,"Ooops, loading image failed.",Toast.LENGTH_SHORT).show();
                                retry.setVisibility(View.VISIBLE);
                                imageView.setVisibility(View.GONE);
                            } else {
                                Log.d(TAG, "loading " + imageUrl + " completed!!!");
                            }
                        }
                    });

        }

        public Uri getLocalBitmapUri(PhotoView imageView,String reddit_id) {
            // Extract Bitmap from ImageView drawable
            Drawable drawable = imageView.getDrawable();
            Bitmap bmp = null;
            if (drawable instanceof BitmapDrawable){
                bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            } else {
                bmp = Ion.with(imageView).getBitmap();
            }

            // Store image to default external storage directory
            Uri bmpUri = null;
            try {
                File file =  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "funswipe_" + reddit_id + ".jpeg");

                FileOutputStream out = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.close();

                bmpUri = Uri.fromFile(file);
                Log.d(TAG,"bmp Uri :"+bmpUri);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return bmpUri;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        private float getContentFontSize(int len) {
            if (len <= 50) return (40);
            else if (len <= 100) return (32);
            else if (len <= 250) return (24);
            else if (len <= 500) return (20);
            else return (14);
        }

        private float getTitleFontSize(int len) {
            if (len <= 30) return (40);
            else return (30);
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
        }
    }

}
