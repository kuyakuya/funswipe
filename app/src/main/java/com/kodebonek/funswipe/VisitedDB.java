package com.kodebonek.funswipe;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

/**
 * Created by bayufa on 4/2/2015.
 */
public class VisitedDB {

    static VisitedDB instance = null;
    String FILENAME = "visited.txt";
    Context mContext;
    HashSet<String> visited_list = new HashSet<String>();

    protected VisitedDB() { }

    public static VisitedDB getInstance() {
        if (instance == null)
            instance = new VisitedDB();
        return(instance);
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void load() {

        FileInputStream fi = null;
        try {

            Log.d("app","loading visited data from "+FILENAME);

            fi = mContext.getApplicationContext().openFileInput(FILENAME);
            DataInputStream data_input = new DataInputStream(fi);
            BufferedReader buffer = new BufferedReader(new InputStreamReader(data_input));
            String str_line;
            while ((str_line = buffer.readLine()) != null) {
                str_line = str_line.trim();
                if ((str_line.length()!=0)) {
                    visited_list.add(str_line);
                }
            }

            Log.d("app","found visited data: "+visited_list.size());

        } catch (FileNotFoundException e) {
            Log.e("app", e.getMessage());
        } catch (IOException e) {
            Log.e("app", e.getMessage());
        }
    }

    void save() {

        FileOutputStream fo = null;
        try {
            fo = mContext.getApplicationContext().openFileOutput(FILENAME,Context.MODE_PRIVATE);

            for (String id: visited_list) {
                fo.write(id.getBytes());
                fo.write("\n".getBytes());
            }
            fo.close();

            Log.d("app","save "+visited_list.size()+" entries to file");

        } catch (FileNotFoundException e) {
            Log.e("app", e.getMessage());
        } catch (IOException e) {
            Log.e("app", e.getMessage());
        }
    }

    public boolean isVisited(String reddit_id) {
        return (visited_list.contains(reddit_id));
    }

    public void add(String reddit_id) {
        visited_list.add(reddit_id);
        save();
    }

    public void clear() {
        visited_list.clear();
        save();
        Log.d("app","clear visited data !!");
    }
}
