package com.kodebonek.funswipe;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

public class BookmarkDB {

    static BookmarkDB instance = null;
    String FILENAME = "favourite.txt";
    Context mContext;
    HashSet<String> bookmark_list = new HashSet<String>();

    protected BookmarkDB() { }

    public static BookmarkDB getInstance() {
        if (instance == null)
            instance = new BookmarkDB();
        return(instance);
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void load() {

        FileInputStream fi = null;
        try {

            Log.d("app","loading favourite data from "+FILENAME);

            fi = mContext.getApplicationContext().openFileInput(FILENAME);
            DataInputStream data_input = new DataInputStream(fi);
            BufferedReader buffer = new BufferedReader(new InputStreamReader(data_input));
            String str_line;
            while ((str_line = buffer.readLine()) != null) {
                str_line = str_line.trim();
                if ((str_line.length()!=0)) {
                    bookmark_list.add(str_line);
                }
            }

            Log.d("app","found favourite data: "+bookmark_list.size());

        } catch (FileNotFoundException e) {
            Log.e("app", e.getMessage());
        } catch (IOException e) {
            Log.e("app", e.getMessage());
        }
    }

    void save() {

        FileOutputStream fo = null;
        try {
            fo = mContext.getApplicationContext().openFileOutput(FILENAME,Context.MODE_PRIVATE);

            for (String id: bookmark_list) {
                fo.write(id.getBytes());
                fo.write("\n".getBytes());
            }
            fo.close();

            Log.d("app","save "+bookmark_list.size()+" entries to favourite");

        } catch (FileNotFoundException e) {
            Log.e("app", e.getMessage());
        } catch (IOException e) {
            Log.e("app", e.getMessage());
        }
    }

    public boolean isBookmarked(String reddit_id) {
        return (bookmark_list.contains(reddit_id));
    }

    public void fave(String reddit_id) {
        bookmark_list.add(reddit_id);
        save();
    }

    public void unfave(String reddit_id) {
        bookmark_list.remove(reddit_id);
        save();
    }

    public void clear() {
        bookmark_list.clear();
        save();
        Log.d("app","clear favourite !!");
    }

    public String getListForQuery(int start,int howmany) {
        String ret = "";
        if (bookmark_list.size()<=start) return("");

        Object[] array = bookmark_list.toArray();
        for (int i=start; i < array.length; i++) {
            ret += (String) array[i];
            if (i < array.length - 1) ret += ",";
        }

        return(ret);
    }
}
