package com.kodebonek.funswipe;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Date;

public class RaterLogic {

    static class UserRateState {
        static int NOT_SHOWED_YET = 0;
        static int USER_LIKE_AND_RATE_THE_APP = 1;
        static int USER_LIKE_BUT_WONT_RATE = 2;
        static int USER_DISLIKE_AND_SEND_FEEDBACK = 3;
        static int USER_DISLIKE_BUT_WONT_FEEDBACK = 4;
    }

    static RaterLogic instance = null;
    Context mContext;
    long startDate = 0;
    long currentDate = 0;
    static String TAG = "RaterLogic";
    int state;
    final int DAYS_TO_SHOW_RATER = 3;

    protected RaterLogic() {
    }

    public static RaterLogic getInstance() {
        if (instance == null)
            instance = new RaterLogic();
        return(instance);
    }

    void init() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        startDate = settings.getLong("start_date", 0);
        if (startDate==0) {
            //oops.no data yet ? first install ?
            startDate = (new Date().getTime()/(24*60*60*1000));
            Log.d(TAG, "new startDate set = " + startDate);

            settings.edit().putLong("start_date",startDate).commit();
        } else {
            Log.d(TAG,"startDate from settings ="+startDate);
        }

        currentDate = (new Date().getTime()/(24*60*60*1000));
        state = settings.getInt("rater_state",UserRateState.NOT_SHOWED_YET);
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public boolean isRaterVisible() {

        //dont ever show the rate again if already choose
        if (state!=UserRateState.NOT_SHOWED_YET)
            return(false);

        //show the rater if startdate
        if (currentDate > startDate + DAYS_TO_SHOW_RATER ) {
            return(true);
        }

        return(false);
    }

    public void userRateTheApp(boolean actionResult) {
        if (actionResult==true)
            state = UserRateState.USER_LIKE_AND_RATE_THE_APP;
        else
            state = UserRateState.USER_LIKE_BUT_WONT_RATE;

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        settings.edit().putInt("rater_state",state).commit();
    }
    public void userSendFeedback(boolean actionResult) {
        if (actionResult==true)
            state = UserRateState.USER_DISLIKE_AND_SEND_FEEDBACK;
        else
            state = UserRateState.USER_DISLIKE_BUT_WONT_FEEDBACK;

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        settings.edit().putInt("rater_state",state).commit();
    }

    public void extendPeriod() {
        startDate = currentDate + 1;
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        settings.edit().putLong("start_date", startDate).commit();
    }

}
