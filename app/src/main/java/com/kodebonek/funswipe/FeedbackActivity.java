package com.kodebonek.funswipe;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kodebonek.funswipe.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

public class FeedbackActivity extends ActionBarActivity {

    static String TAG = "FeedbackActivity";
    private ProgressDialog mIndeterminateProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        Toolbar bar = (Toolbar) findViewById(R.id.toolbar);
        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        bar.setBackgroundColor(getResources().getColor(R.color.app_actionbar_background));

        TextView tv = (TextView) findViewById(R.id.textView);
        tv.setTypeface(MainActivity.contentTypeFace);
        tv.setText("Use form below to send us any bug reports, comments, or suggestions regarding this app.");
//                "If possible, please provide specific details that can help improve the quality and performance of the app. Thanks!");

        final EditText et = (EditText) findViewById(R.id.editText);
        et.setTypeface(MainActivity.contentTypeFace);

        Button btnSend = (Button) findViewById(R.id.buttonSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send the data
                String OS = Build.VERSION.RELEASE+ " (SDK: "+Build.VERSION.SDK_INT+")";
                String phone = Build.MANUFACTURER+" "+Build.MODEL;
                String content = et.getText().toString();

                String URL = "http://api.kodebonek.com/funswipe/feedback.php";
                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                params.add("os",OS);
                params.add("phone",phone);
                params.add("feedback",content);
                setProgressVisible(true);
                client.post(URL,params,new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject jsonObject) {
                        setProgressVisible(false);
                        Toast.makeText(getApplicationContext(),"Done! i'll check it ASAP. Thanks",Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        setProgressVisible(false);
                        Toast.makeText(getApplicationContext(), "Error: " + statusCode + " " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e(TAG, statusCode + " " + throwable.getMessage());
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        setProgressVisible(false);
                        Toast.makeText(getApplicationContext(), "Error: " + statusCode + " " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e(TAG, statusCode + " " + throwable.getMessage());
                    }
                });
            }
        });

        Spinner spin = (Spinner) findViewById(R.id.spinnerFeedback);
        spin.setAdapter(ArrayAdapter.createFromResource(this,
                R.array.menu_feedback,
                android.R.layout.simple_spinner_dropdown_item));

    }

    private void setupIndeterminateProgressDialog() {
        mIndeterminateProgressDialog = new ProgressDialog(FeedbackActivity.this,R.style.MyProgressDialog);
        mIndeterminateProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mIndeterminateProgressDialog.setIndeterminate(true);
        mIndeterminateProgressDialog.setCancelable(false);
    }

    private void setProgressVisible(boolean visible) {

        if (visible) {

            setupIndeterminateProgressDialog();
            int rotation = getWindowManager().getDefaultDisplay().getRotation();

            switch(rotation) {
                case Surface.ROTATION_180:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                    break;
                case Surface.ROTATION_270:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                    break;
                case  Surface.ROTATION_0:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
                case Surface.ROTATION_90:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
            }

            mIndeterminateProgressDialog.show();
        } else {
            //unlock
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

            if (mIndeterminateProgressDialog!=null)
                mIndeterminateProgressDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_feedback, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
